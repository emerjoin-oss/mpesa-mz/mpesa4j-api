# Mpesa4j
An __Unofficial__, synchronous Java native API to Integrate __your backend__ with __Vodafone Mpesa Mozambique__, based on their Open-API.

## Features
* Synchronous - less trouble for you trust me
* Type safety for all Params - less error prone
* Logging for better troubleshooting 
* Web proxy configuration
* Simplicity - far simpler than the __Mpesa-API__
* Exceptions to handle errors - forget the status codes and catch exceptions
* Configurations centralized - one single object to rule the API behaviour
* Out of the box CDI Support - just inject it
* Well documented API - you are looking at it
* Abstraction of the MPesa REST-API - this is not a Rest-Client, not at all
* Maximum response wait configuration - don't wait forever for a response
* Proactive validation of parameters - we validate before we send to MPesa. Definitely not just a rest client 
* We __auto-generate__ references for you so that you don't have to
* We hide the clutter parameters that the Mpesa API requires you to pass

## Assumptions
* Phone numbers have 9 digits and start with either _84_ or _85_ prefix.

## Setting up your project

### Maven Repository
Use the emerjoin oss repository
```xml
    <repositories>
        <repository>
            <id>emerjoin-oss</id>
            <name>maven</name>
            <url>https://pkg.emerjoin.org/oss</url>
            <snapshots>
                <enabled>true</enabled>
                <updatePolicy>daily</updatePolicy>
            </snapshots>
        </repository>
    </repositories>
```

#### Maven dependencies
Add the following dependencies to your project:


### Dependencies
```xml
    <dependencies>
        ...
        <dependency>
            <groupId>org.emerjoin.mpesa</groupId>
            <artifactId>mpesa4j-api</artifactId>
            <version>1.0.0-SNAPSHOT</version>
        </dependency>
 
        <dependency>
            <groupId>org.emerjoin.mpesa</groupId>
            <artifactId>mpesa4j-impl</artifactId>
            <version>1.0.0-SNAPSHOT</version>
        </dependency> 
        ...
    </dependencies>
```


## Configurations
The __Mpesa4j__ API centralizes it's configurations in a single object denominated __Configuration__.<br>
What are these configurations?
* The location of the M-Pesa-API server
* A proxy server through which the requests must go.
* Indication of whether encryption shall be applied when communicating with the M-Pesa API server.
* The maximum amount of time your application is willing to wait for the M-Pesa API to respond to a Request.
* All the M-pesa-API __parameters__ that are expected to have the same value across different requests.<br><br>
The code snippet below gives an example of how to __set__ and __apply__ configurations.
```java
//Creating and applying configurations
Configuration config = Configuration.builder()
    .port(18352)
    .hostname("api.sandbox.vm.co.mz")
    .maxResponseWait(10000)//10 seconds
    .ssl(true)
    .sslTrustAll(true)
    .token("BViUo6PFiawnTeKkRMt+IaCa2xvwIDu")
    .proxy("127.0.0.1",8081) //Optional
    .serviceProviderCode("171717")
    //.initiatorIdentifier("MPesa2018")
    //.securityCredential("MPesa2019")
    .build();
config.apply();
```
Instead of providing the __token__, you can provide the PublicKey and the ApiKey, we will figure out the token:
```java
URL publicKeyURL = //whatever
Configuration config = Configuration.builder()
    //...
    .publicKey(publicKeyURL) //You can provide the Key Content or a File
    .apiKey("b30c9d376")
    //...
    .build();
```

## MpesaService
All the <b>M-Pesa</b> APIs are wrapped into an abstraction denominated <b>MpesaService</b>.

### Obtaining Reference

Default approach:
```java
    MpesaService service = Mpesa4j.service();
```

CDI Approach:
```java
    @Inject
    private MpesaService service;
```

## Mpesa Operations
You are about to learn how to perform Mpesa operations using the __MpesaService__.
But lets start by introducing you to some basic MPesa concepts:
* Customer - an individual with an M-Pesa Account.
* Business - an Institution/Company with an M-Pesa account.

### Request Customer Payment
Let's see how to request a Pizza payment from a __customer__. We charge 100MT for a Pizza.
```java
try{
    Ref mpesaRef = service.submit(
        C2BPaymentRequest.of(Amount.of(200.0))
             .from(new MobileNumber("842500101"))
                 .build();
    );
    //TODO: Maybe store mpesaRef in order to be able to reverse the payment or even provide that it was made, should there be any issues on the Mpesa side.
}catch(IrregularCustomerException ex){
    //Tell the customer to regularize his Mpesa account
}catch(InsufficientBalanceException ex){
    //Tell the customer to fund his/her account.
}catch(CancelledByCustomerException ex){
    //The customer cancelled the payment. Show payment cancellation message.
}catch(PaymentApprovalTimeoutException ex){
    //Customer did not approve the payment within the time expected by Mpesa
    //Send the request again
}catch(OperationTimeoutException ex){
    //Mpesa took to long to respond. Check the status of the transaction
    //and try again if the transaction failed.
}catch(MpesaInternalException ex){
    //An error internal to Mpesa occurred. Check the status of the transaction
    //and try again if the payment is confirmed to have failed.
}catch(MpesaServiceException ex){
    //You cant recover from this. Show a generic error message.
    //Trying again most probably won't work. Human intervention recommended.
}
```

### Reverse Payment
You might by mistake charge a customer more than you should and you definitely will want
him/her to have the overcharged amount back. The other possibility is that the customer
might want to cancel his/her order. It could be even worst, you might not be able to fulfil the customer's request while he/she already paid. You will definitely want to return the funds. 
<br><br>
The __Reverse Payment__ operation is all about helping you in these situations.<br><br>
The only thing you need to have in order to be able to reverse a payment is the respective __mpesaRef__.

#### Full amount reverse
Let's reverse a pizza payment, the whole amount:
```java
 try{
    Ref mpesaRef = service.execute(PaymentReverse.fullAmount(
            paymentMpesaRef));
    //TODO: Maybe store the returned mpesaRef in order to be able to confront Mpesa should there be any issues.
}catch(MpesaInternalException ex){
    //An error internal to Mpesa occurred. Check the status of the transaction
    //and try again if the reverse is confirmed to have failed.
}catch(OperationTimeoutException ex){
    //Mpesa took to long to respond. Check the status of the transaction
    //and try again if the transaction failed.
}catch(MpesaServiceException ex){
    //You cant recover from this. Show a generic error message.
    //Trying again most probably won't work. Human intervention recommended.
}
```

#### Partial amount reverse
Let's reverse a pizza payment, not the whole amount. Only the 10MT that we overcharged:
```java
 try{
    Ref mpesaRef = service.execute(PaymentReverse.partialAmount(paymentMpesaRef,
            Amount.of(10.0)));
}catch(MpesaInternalException ex){
    //An error internal to Mpesa occurred. Check the status of the transaction
    //and try again if the reverse is confirmed to have failed.
}catch(OperationTimeoutException ex){
    //Mpesa took to long to respond. Check the status of the transaction
    //and try again if the transaction failed.
}catch(MpesaServiceException ex){
    //You cant recover from this. Show a generic error message.
    //Trying again most probably won't work. Human intervention recommended.
}
```

### Get Transaction status
Every transaction has a __Ref__ and that's all you need to find out it's current status. This is why it is important to persist the __Ref__ before you submit/execute the transaction.
It could be a __PaymentRequest__ or even __PaymentReverse__. As long as you have the __Ref__ you are good to go:
```java
try{
    Ref transactionRef = //a previously started transaction
    Optional<TransactionStatus> status = service.getTransactionStatus(transactionRef);
    if(!status.isPresent()){
        //Unknown transaction
    }
}catch(MpesaInternalException ex){
    //An error internal to Mpesa occurred.
}catch(OperationTimeoutException ex){
    //Mpesa took to long to respond. Just try again
}catch(MpesaServiceException ex){
    //Show a generic error message.
    //Trying again most probably won't work. Human intervention recommended.
}
```
