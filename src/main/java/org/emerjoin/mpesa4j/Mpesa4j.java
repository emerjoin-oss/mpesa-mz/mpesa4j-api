package org.emerjoin.mpesa4j;

import java.util.Iterator;
import java.util.ServiceLoader;

/**
 * @author Mario Junior.
 */
public class Mpesa4j {

    public static MpesaService service(){
        Iterator<MpesaService> iterator = ServiceLoader.load(MpesaService.class).iterator();
        if(!iterator.hasNext())
            throw new Mpesa4jException(String.format("There is no implementation of the %s service available",
                    MpesaService.class.getSimpleName()));
        return iterator.next();
    }

}
