package org.emerjoin.mpesa4j;

import org.hashids.Hashids;

import java.time.LocalDateTime;
import java.time.temporal.ChronoField;

/**
 * A string of a maximum of 20 characters without blank spaces nor special characters
 */
public class Ref {

    private String value;

    private Ref(String value){
        CommonValidators.validateRef(value, "ref");
        this.value = value;
    }

    public static Ref auto(){
        LocalDateTime dateTime = LocalDateTime.now();
        int micro = dateTime.get(ChronoField.MICRO_OF_SECOND);
        int nano = dateTime.get(ChronoField.NANO_OF_SECOND);
        Hashids hashids = new Hashids();
        return new Ref(hashids.encode(dateTime.get(ChronoField.MILLI_OF_DAY),
                micro,
                nano));
    }

    public static Ref val(String value){
        return new Ref(value);
    }

    public String toString(){
        return value;
    }

}
