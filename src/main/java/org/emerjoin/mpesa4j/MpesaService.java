package org.emerjoin.mpesa4j;

import org.emerjoin.mpesa4j.c2b.C2BPaymentRequest;
import org.emerjoin.mpesa4j.exception.MpesaServiceException;

import java.util.Optional;

/**
 * Service that wraps all the <b>Mpesa-API</b> .
 * @author Mario Junior.
 */
public interface MpesaService {

    Ref submit(C2BPaymentRequest request) throws MpesaServiceException;
    Ref execute(PaymentReverse reverse) throws MpesaServiceException;
    Optional<TransactionStatus> getTransactionStatus(Ref transactionRef) throws MpesaServiceException;

}
