package org.emerjoin.mpesa4j;

import java.util.Optional;

/**
 * Represents the Intention of reversing a previously executed Payment.
 * @author Mario Junior.
 */
public class PaymentReverse implements Transaction {

    private Ref transactionRef;
    private Ref paymentRef;
    private Amount amount;

    /**
     * Creates a new full amount {@link PaymentReverse} with auto-generated transaction-Ref.
     * @param paymentRef Reference of the payment to be reversed.
     */
    public PaymentReverse(Ref paymentRef){
        this(paymentRef, null, Ref.auto());
    }

    /**
     * Creates a new partial amount {@link PaymentReverse} with auto-generated transaction-Ref.
     * @param paymentRef Reference of the payment to be reversed.
     * @param amount Partial amount to be reversed.
     */
    public PaymentReverse(Ref paymentRef, Amount amount){
        this(paymentRef, amount, Ref.auto());
    }

    /**
     * Creates a new full amount {@link PaymentReverse}.
     * @param paymentRef Reference of the payment to be reversed.
     * @param transactionRef a unique identifier for the reverse operation.
     */
    public PaymentReverse(Ref paymentRef, Ref transactionRef){
        this(paymentRef, null, transactionRef);
    }

    /**
     * Creates a new {@link PaymentReverse}.
     * @param paymentRef Reference of the payment to be reversed.
     * @param transactionRef a unique identifier for the reverse operation.
     * @param amount the partial amount to be reversed. If null then the full payment amount is reverse
     */
    public PaymentReverse(Ref paymentRef, Amount amount, Ref transactionRef){
        CommonValidators.requireNotNull("paymentRef",paymentRef);
        CommonValidators.requireNotNull("operationRef",transactionRef);
        this.transactionRef = transactionRef;
        this.paymentRef = paymentRef;
        this.amount = amount;
    }

    public Ref getTransactionRef() {
        return transactionRef;
    }

    public Ref getPaymentRef() {
        return paymentRef;
    }

    public Optional<Amount> getPartialAmount() {
        return Optional.ofNullable(
                amount);
    }

    public boolean isPartial(){
        return amount!=null;
    }

    public static PaymentReverse partialAmount(Ref paymentRef, Amount amount){
        CommonValidators.requireNotNull("paymentRef",paymentRef);
        CommonValidators.requireNotNull("amount",amount);
        return new PaymentReverse(paymentRef,amount);
    }

    public static PaymentReverse partialAmount(Ref paymentRef, Amount amount, Ref transactionRef){
        CommonValidators.requireNotNull("paymentRef",paymentRef);
        CommonValidators.requireNotNull("amount",amount);
        CommonValidators.requireNotNull("transactionRef",transactionRef);
        return new PaymentReverse(paymentRef,amount,
                transactionRef);
    }

    public static PaymentReverse fullAmount(Ref paymentRef){
        CommonValidators.requireNotNull("paymentRef",paymentRef);
        return new PaymentReverse(paymentRef);
    }

    public static PaymentReverse fullAmount(Ref paymentRef, Ref transactionRef){
        CommonValidators.requireNotNull("paymentRef",paymentRef);
        CommonValidators.requireNotNull("transactionRef",transactionRef);
        return new PaymentReverse(paymentRef,transactionRef);
    }

}
