package org.emerjoin.mpesa4j.exception;

import org.emerjoin.mpesa4j.Mpesa4jException;

/**
 * @author Mario Junior.
 */
public class InitializationException extends Mpesa4jException {

    public InitializationException(String message, Throwable cause) {
        super(message, cause);
    }

    public InitializationException(String message) {
        super(message);
    }
}
