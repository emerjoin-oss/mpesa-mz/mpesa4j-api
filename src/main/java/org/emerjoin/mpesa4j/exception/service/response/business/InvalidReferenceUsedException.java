package org.emerjoin.mpesa4j.exception.service.response.business;

import org.emerjoin.mpesa4j.exception.service.response.BusinessException;

public class InvalidReferenceUsedException extends BusinessException {

    public InvalidReferenceUsedException(){
        super("Invalid Reference Used","INS-14");
    }

}
