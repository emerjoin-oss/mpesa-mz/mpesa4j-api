package org.emerjoin.mpesa4j.exception.service.response.business;

import org.emerjoin.mpesa4j.Ref;
import org.emerjoin.mpesa4j.exception.service.response.BusinessException;

public class InvalidTransactionRefException extends BusinessException {

    public InvalidTransactionRefException(Ref ref) {
        super(String.format("Invalid Transaction Reference: \"%s\". Length Should Be Between 1 and 20",ref),"INS-17");
    }

}
