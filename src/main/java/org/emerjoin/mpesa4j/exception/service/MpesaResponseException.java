package org.emerjoin.mpesa4j.exception.service;

import org.emerjoin.mpesa4j.exception.MpesaServiceException;

import java.util.Optional;

public class MpesaResponseException extends MpesaServiceException {

    private String code;

    public MpesaResponseException(String message, String code) {
        super(message+"."+(code!=null&&!code.isEmpty() ? " Response Code: "+code : ""));
        this.code = code;
    }

    public Optional<String> getCode() {
        return Optional.ofNullable(code);
    }
}
