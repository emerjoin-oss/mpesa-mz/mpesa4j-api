package org.emerjoin.mpesa4j.exception.service;

import org.emerjoin.mpesa4j.exception.MpesaServiceException;

/**
 * Indicates that M-Pesa could not reply within the maxResponseWait time. Do not assume that the Request failed, because
 * it might have finished successfully. Check the transaction status before further action.
 * @author Mario Junior.
 */
public class OperationTimeoutException extends MpesaServiceException {

    public OperationTimeoutException(long milliseconds) {
        super(String.format("The Mpesa System did not respond within %d milliseconds",
                milliseconds));
    }

    public OperationTimeoutException() {
        super("The Mpesa System did not respond within the expected time");
    }

}
