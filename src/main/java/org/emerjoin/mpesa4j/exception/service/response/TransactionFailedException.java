package org.emerjoin.mpesa4j.exception.service.response;

import org.emerjoin.mpesa4j.exception.service.MpesaResponseException;

public class TransactionFailedException extends MpesaResponseException {

    public TransactionFailedException() {
        super("Transaction failed", "INS-6");
    }
}
