package org.emerjoin.mpesa4j.exception.service.response.business;

/**
 * @author Mario Junior.
 */
public class InvalidCustomerMsisdnException extends InvalidTransactionParamException {

    public InvalidCustomerMsisdnException(String msisdn){
        super("Invalid MSISDN value provided: "+msisdn,"INS-2051");
    }

}
