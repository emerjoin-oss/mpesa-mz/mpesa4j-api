package org.emerjoin.mpesa4j.exception.service.response.business;

import org.emerjoin.mpesa4j.exception.service.response.BusinessException;

public class InvalidTransactionParamException extends BusinessException {

    public InvalidTransactionParamException(String message, String code) {
        super(message,code);
    }
}
