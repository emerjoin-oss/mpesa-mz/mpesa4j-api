package org.emerjoin.mpesa4j.exception.service.response;

import org.emerjoin.mpesa4j.exception.MpesaServiceException;
import org.emerjoin.mpesa4j.exception.service.MpesaResponseException;

/**
 * Reports an error that occurred internally at the Mpesa System.
 * @author Mario Junior.
 */
public class MpesaInternalException extends MpesaResponseException {

    public MpesaInternalException() {
        super("Mpesa Internal error","INS-1");
    }

}
