package org.emerjoin.mpesa4j.exception.service.response.config;

import org.emerjoin.mpesa4j.exception.service.response.InvalidConfigException;

public class InvalidInitiatorIdentifierException extends InvalidConfigException {

    public InvalidInitiatorIdentifierException(String value) {
        super("Invalid initiator identifier used: "+value,"INS-24");
    }
}
