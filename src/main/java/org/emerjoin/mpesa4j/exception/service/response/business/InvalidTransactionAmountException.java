package org.emerjoin.mpesa4j.exception.service.response.business;

/**
 * @author Mario Junior.
 */
public class InvalidTransactionAmountException extends InvalidTransactionParamException {

    public InvalidTransactionAmountException(double amount){
        super(String.format("Invalid amount value provided: %.2f",amount),"INS-15");
    }

}
