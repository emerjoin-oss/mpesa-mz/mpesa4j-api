package org.emerjoin.mpesa4j.exception.service;

import org.emerjoin.mpesa4j.exception.MpesaServiceException;

/**
 * Represents an Mpesa4j implementation internal error.
 * @author Mario Junior.
 */
public class InfrastructureException extends MpesaServiceException {

    public InfrastructureException(String message, Throwable cause) {
        super(message, cause);
    }

    public InfrastructureException(String message) {
        super(message);
    }
}
