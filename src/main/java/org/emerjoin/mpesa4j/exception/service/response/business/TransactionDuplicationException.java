package org.emerjoin.mpesa4j.exception.service.response.business;

import org.emerjoin.mpesa4j.exception.service.response.BusinessException;
import org.emerjoin.mpesa4j.Transaction;

/**
 * There is a recent record a similar {@link Transaction} involving the same entities regarding the same amount
 * @author Mario Junior.
 */
public class TransactionDuplicationException extends BusinessException {

    public TransactionDuplicationException() {
        super("There is a recent record a similar transaction involving the same entities regarding the same amount","INS-10");
    }

}
