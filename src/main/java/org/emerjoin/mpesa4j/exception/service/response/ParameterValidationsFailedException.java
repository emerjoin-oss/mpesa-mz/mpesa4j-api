package org.emerjoin.mpesa4j.exception.service.response;

import org.emerjoin.mpesa4j.exception.service.MpesaResponseException;

public class ParameterValidationsFailedException extends MpesaResponseException {

    public ParameterValidationsFailedException(String message) {
        super("Parameter validations failed", "INS-21");
    }
}
