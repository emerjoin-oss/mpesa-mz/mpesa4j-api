package org.emerjoin.mpesa4j.exception.service;

import org.emerjoin.mpesa4j.exception.MpesaServiceException;
import org.emerjoin.mpesa4j.exception.service.InfrastructureException;
import org.emerjoin.mpesa4j.exception.service.MpesaResponseException;
import org.emerjoin.mpesa4j.exception.service.response.business.InvalidTransactionParamException;

/**
 * Indicates deviation from the Mpesa-API protocol. The protocol is made of rules around how the business messages from Your application to Mpesa are
 * formatted and transmitted. The status codes are also part of the protocol. If the M-Pesa-API protocol changes and Mpesa4j is not updated to conform, then this exception is going to be see.
 * Protocol violations might cause {@link InfrastructureException}s.
 * @author Mario Junior.
 */
public class ProtocolViolationException extends MpesaServiceException {

    public ProtocolViolationException(String message) {
        super(message);
    }

}
