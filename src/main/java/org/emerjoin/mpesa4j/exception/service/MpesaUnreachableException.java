package org.emerjoin.mpesa4j.exception.service;

import org.emerjoin.mpesa4j.exception.MpesaServiceException;

/**
 * Indicates that the M-Pesa API server cannot be reached.
 * It doesn't mean the M-Pesa API server is not available. If you indicated a proxy server in the configurations, it could be that it is actually the proxy server that is not available or it could be a network connectivity issue between the Proxy and Mpesa or between your Application and Mpesa, if there is no proxy in-between.
 * @author Mario Junior.
 */
public class MpesaUnreachableException extends MpesaServiceException {

    public MpesaUnreachableException() {
        super("Not able to reach the Mpesa System");
    }

}
