package org.emerjoin.mpesa4j.exception.service.response;

import org.emerjoin.mpesa4j.exception.service.MpesaResponseException;
import org.emerjoin.mpesa4j.exception.service.ProtocolViolationException;

/**
 * @author Mario Junior.
 */
public class InvalidRequestException extends MpesaResponseException {

    public InvalidRequestException() {
        super("Invalid request","INS-13");
    }

}
