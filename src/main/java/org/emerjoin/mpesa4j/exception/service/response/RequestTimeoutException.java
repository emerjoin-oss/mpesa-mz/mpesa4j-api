package org.emerjoin.mpesa4j.exception.service.response;

import org.emerjoin.mpesa4j.exception.service.MpesaResponseException;

public class RequestTimeoutException extends MpesaResponseException {

    public RequestTimeoutException() {
        super("Request timeout","INS-9");
    }
}
