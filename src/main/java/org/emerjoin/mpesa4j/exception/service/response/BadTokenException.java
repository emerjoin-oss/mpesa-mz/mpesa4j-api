package org.emerjoin.mpesa4j.exception.service.response;

import org.emerjoin.mpesa4j.exception.service.MpesaResponseException;

public class BadTokenException extends MpesaResponseException {

    public BadTokenException(String description) {
        super("Bad token used: "+description,null);
    }
}
