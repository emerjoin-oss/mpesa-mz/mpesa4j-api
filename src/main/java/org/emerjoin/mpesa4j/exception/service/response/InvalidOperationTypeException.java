package org.emerjoin.mpesa4j.exception.service.response;

import org.emerjoin.mpesa4j.exception.service.MpesaResponseException;

public class InvalidOperationTypeException extends MpesaResponseException {

    public InvalidOperationTypeException() {
        super("Invalid operation type","INS-22");
    }
}
