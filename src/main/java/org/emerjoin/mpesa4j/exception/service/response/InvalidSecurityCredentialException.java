package org.emerjoin.mpesa4j.exception.service.response;

import org.emerjoin.mpesa4j.exception.service.MpesaResponseException;
import org.emerjoin.mpesa4j.exception.service.response.InvalidConfigException;

public class InvalidSecurityCredentialException extends MpesaResponseException {

    public InvalidSecurityCredentialException() {
        super("Invalid security credential used","INS-25");
    }
}
