package org.emerjoin.mpesa4j.exception.service.response;

import org.emerjoin.mpesa4j.exception.service.MpesaResponseException;

public class TemporaryOverloadingException extends MpesaResponseException {

    public TemporaryOverloadingException(){
        super("Unable to handle the request due to a temporary overloading","INS-16");
    }

}
