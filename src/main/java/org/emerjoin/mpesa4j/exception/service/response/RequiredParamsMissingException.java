package org.emerjoin.mpesa4j.exception.service.response;

import org.emerjoin.mpesa4j.exception.service.MpesaResponseException;
import org.emerjoin.mpesa4j.exception.service.ProtocolViolationException;

/**
 *
 */
public class RequiredParamsMissingException extends MpesaResponseException {

    public RequiredParamsMissingException() {
        super("Required params are missing"," INS-20");
    }
}
