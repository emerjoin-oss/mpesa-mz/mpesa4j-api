package org.emerjoin.mpesa4j.exception.service.response.business;

import org.emerjoin.mpesa4j.Ref;
import org.emerjoin.mpesa4j.exception.service.response.BusinessException;

public class Invalid3rdPartyReferenceException extends BusinessException {

    public Invalid3rdPartyReferenceException(Ref ref) {
        super("Invalid 3rd-Party reference: "+ref, "INS-19");
    }

    public Invalid3rdPartyReferenceException() {
        super("Invalid 3rd-Party reference", "INS-19");
    }

}
