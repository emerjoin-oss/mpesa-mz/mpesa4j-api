package org.emerjoin.mpesa4j.exception.service.response.business;

import org.emerjoin.mpesa4j.exception.service.response.BusinessException;

public class InvalidTransactionIdException extends BusinessException {

    public InvalidTransactionIdException() {
        super("Invalid transaction ID", "INS-18");
    }

}
