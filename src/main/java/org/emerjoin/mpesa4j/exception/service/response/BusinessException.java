package org.emerjoin.mpesa4j.exception.service.response;

import org.emerjoin.mpesa4j.exception.MpesaServiceException;
import org.emerjoin.mpesa4j.exception.service.MpesaResponseException;

/**
 * Reports an exceptional business situation, the preconditions for the business operation to succeed are not met.
 * @author Mario Junior.
 */
public class BusinessException extends MpesaResponseException {

    public BusinessException(String message, String code) {
        super(message,code);
    }

}
