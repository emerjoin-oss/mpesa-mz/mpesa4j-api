package org.emerjoin.mpesa4j.exception.service.response;

import org.emerjoin.mpesa4j.exception.service.MpesaResponseException;

public class InitiatorAuthenticationErrorException extends MpesaResponseException {

    public InitiatorAuthenticationErrorException() {
        super("Initiator authentication error","INS-2001");
    }

}
