package org.emerjoin.mpesa4j.exception.service;

import org.emerjoin.mpesa4j.exception.MpesaServiceException;

/**
 * There is a configuration required in order to perform the requested business operation.
 */
public class RequiredConfigException extends MpesaServiceException {

    public RequiredConfigException(String message) {
        super(message);
    }
}
