package org.emerjoin.mpesa4j.exception.service.response.config;

import org.emerjoin.mpesa4j.exception.service.response.InvalidConfigException;

public class InvalidShortCodeException extends InvalidConfigException {

    public InvalidShortCodeException(String shortCode) {
        super("Invalid short code used: "+shortCode,"INS-13");
    }
}
