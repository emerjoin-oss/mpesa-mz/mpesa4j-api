package org.emerjoin.mpesa4j.exception.service.response;

import org.emerjoin.mpesa4j.exception.service.MpesaResponseException;

/**
 * Indicates that a configuration value was deemed invalid by the <b>Mpesa-API server</b>.
 */
public class InvalidConfigException extends MpesaResponseException {

    public InvalidConfigException(String message, String code) {
        super(message,code);
    }
}
