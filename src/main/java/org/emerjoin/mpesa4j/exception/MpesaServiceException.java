package org.emerjoin.mpesa4j.exception;

import org.emerjoin.mpesa4j.Mpesa4jException;
import org.emerjoin.mpesa4j.MpesaService;

/**
 * The superclass all possible exceptions thrown during the execution of an {@link MpesaService} operation.
 * @author Mario Junior.
 */
public class MpesaServiceException extends Mpesa4jException {

    public MpesaServiceException(String message, Throwable cause){
        super(message,cause);
    }

    public MpesaServiceException(String message){
        super(message);
    }

}
