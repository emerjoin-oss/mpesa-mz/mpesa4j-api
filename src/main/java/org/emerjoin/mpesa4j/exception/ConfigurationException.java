package org.emerjoin.mpesa4j.exception;

import org.emerjoin.mpesa4j.Mpesa4jException;

/**
 * Indicates that something went wrong while setting a configuration value or there is a configuration required in order to
 * perform a requested business operation.
 * @author Mario Junior.
 */
public class ConfigurationException extends Mpesa4jException {

    public ConfigurationException(Throwable cause){
        super("Error found while building Configurations",cause);
    }

    public ConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConfigurationException(String message) {
        super(message);
    }
}
