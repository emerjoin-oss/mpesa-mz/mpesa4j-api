package org.emerjoin.mpesa4j;

import org.apache.commons.io.IOUtils;
import org.emerjoin.mpesa4j.exception.ConfigurationException;

import javax.crypto.Cipher;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Optional;

/**
 * @author Mario Junior.
 */
public class Configuration {

    private int port = 18352;
    private String hostname = "api.sandbox.vm.co.mz";
    private String token;
    private boolean ssl = true;
    private int maxResponseWait = 15000; //15s
    private boolean sslTrustAll = false;
    private WebProxy webProxy = null;
    private String serviceProviderCode = "171717";
    private String initiatorIdentifier = null;
    private String securityCredential = null;

    private static Configuration CURRENT = new Configuration();

    private Configuration(){

    }

    public int getPort() {
        securityCredential = securityCredential;return port;
    }

    public String getHostname() {
        return hostname;
    }

    public String getToken() {
        return token;
    }

    public boolean isSsl() {
        return ssl;
    }

    public boolean isSslTrustAll() {
        return sslTrustAll;
    }

    public int getMaxResponseWait() {
        return maxResponseWait;
    }

    public Optional<WebProxy> getWebProxy(){
        return Optional.ofNullable(
                webProxy);
    }

    public String getServiceProviderCode() {
        return serviceProviderCode;
    }

    public String getInitiatorIdentifier() {
        return initiatorIdentifier;
    }

    public String getSecurityCredential() {
        return securityCredential;
    }

    public void setPort(int port) {
        if(port<1)
            throw new IllegalArgumentException("port must not be less than 1");
        this.port = port;
    }

    public void setHostname(String hostname) {
        if(hostname==null||hostname.isEmpty())
            throw new IllegalArgumentException("hostname must not be null nor empty");
        this.hostname = hostname;
    }

    public void setToken(String token) {
        if(token==null||token.isEmpty())
            throw new IllegalArgumentException("token must not be null nor empty");
        this.token = token;
    }

    public void setSsl(boolean ssl) {
        this.ssl = ssl;
    }

    public void setMaxResponseWait(int maxResponseWait) {
        if(maxResponseWait<1)
            throw new IllegalArgumentException("maxResponseWait must not be less than 1");
        this.maxResponseWait = maxResponseWait;
    }

    public void setSslTrustAll(boolean sslTrustAll) {
        this.sslTrustAll = sslTrustAll;
    }

    public void setWebProxy(WebProxy webProxy) {
        if(webProxy==null)
            throw new IllegalArgumentException("webProxy must be null");
        this.webProxy = webProxy;
    }

    public void setServiceProviderCode(String serviceProviderCode) {
        if(serviceProviderCode==null||serviceProviderCode.isEmpty())
            throw new IllegalArgumentException("serviceProviderCode must not be null nor empty");
        this.serviceProviderCode = serviceProviderCode;
    }

    public void setInitiatorIdentifier(String initiatorIdentifier) {
        if(initiatorIdentifier==null||initiatorIdentifier.isEmpty())
            throw new IllegalArgumentException("initiatorIdentifier must not be null nor empty");
        this.initiatorIdentifier = initiatorIdentifier;
    }

    public void setSecurityCredential(String securityCredential) {
        if(securityCredential==null||securityCredential.isEmpty())
            throw new IllegalArgumentException("securityCredential must not be null nor empty");
        this.securityCredential = securityCredential;
    }

    public Configuration clone(){
        Configuration cloned = new Configuration();
        cloned.serviceProviderCode = serviceProviderCode;
        cloned.initiatorIdentifier = initiatorIdentifier;
        cloned.securityCredential = securityCredential;
        cloned.webProxy = webProxy;
        cloned.ssl = ssl;
        cloned.sslTrustAll = sslTrustAll;
        cloned.port = port;
        cloned.hostname = hostname;
        cloned.maxResponseWait = maxResponseWait;
        cloned.token = token;
        return cloned;
    }

    public void apply(){
        CURRENT = this;
    }

    public static Configuration current(){
        return CURRENT;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder {

        private Configuration config = new Configuration();
        private String apiKey;
        private byte[] publicKeyContent;

        private Builder(){

        }

        public Builder port(int port){
            if(port<1)
                throw new IllegalArgumentException("port must be higher than zero");
            config.port = port;
            return this;
        }

        public Builder hostname(String hostname){
            if(hostname==null||hostname.isEmpty())
                throw new IllegalArgumentException("hostname must not be null nor empty");
            config.hostname = hostname;
            return this;
        }

        public Builder maxResponseWait(int wait){
            config.setMaxResponseWait(wait);
            return this;
        }

        public Builder ssl(boolean ssl){
            config.setSsl(ssl);
            return this;
        }

        public Builder sslTrustAll(boolean trustAll){
            config.setSslTrustAll(trustAll);
            return this;
        }

        public Builder token(String token){
            config.setToken(token);
            return this;
        }

        public Builder publicKey(URL publicKeyUrl){
            if(publicKeyUrl==null)
                throw new IllegalArgumentException("publicKeyUrl must not be null");
            try {
                this.publicKeyContent = IOUtils.toByteArray(publicKeyUrl);
            }catch (IOException ex){
                throw new Mpesa4jException("error reading public Key: "+publicKeyUrl,
                        ex);
            }
            return this;
        }

        public Builder publicKey(String keyContent){
            if(keyContent==null||keyContent.isEmpty())
                throw new IllegalArgumentException("keyContent must not be null");
            this.publicKeyContent = keyContent.getBytes();
            return this;
        }

        public Builder publicKey(File keyFile){
            if(keyFile==null||!keyFile.exists()||keyFile.isDirectory())
                throw new IllegalArgumentException("keyFile must point to an existing file");
            try {
                this.publicKeyContent = IOUtils.toByteArray(new FileInputStream(keyFile));
            }catch (IOException ex){
                throw new Mpesa4jException("error reading key file: "+keyFile.getAbsolutePath(),
                        ex);
            }
            return this;
        }

        public Builder proxy(String hostname, int port){
            if(hostname==null||hostname.isEmpty())
                throw new IllegalArgumentException("hostname must not be null nor empty");
            if(port<1)
                throw new IllegalArgumentException("port must not be less than 1");
            this.config.setWebProxy(new WebProxy(hostname, port));
            return this;
        }

        public Builder apiKey(String apiKey){
            if(apiKey==null||apiKey.isEmpty())
                throw new IllegalArgumentException("apiKey reference must not be null nor empty");
            this.apiKey = apiKey;
            return this;
        }

        public Builder serviceProviderCode(String serviceProviderCode){
            if(serviceProviderCode==null||serviceProviderCode.isEmpty())
                throw new IllegalArgumentException("serviceProviderCode must not be null nor empty");
            this.config.setServiceProviderCode(serviceProviderCode);
            return this;
        }

        public Builder initiatorIdentifier(String initiatorIdentifier){
            if(initiatorIdentifier==null||initiatorIdentifier.isEmpty())
                throw new IllegalArgumentException("initiatorIdentifier must not be null nor empty");
            this.config.setInitiatorIdentifier(initiatorIdentifier);
            return this;
        }

        public Builder securityCredential(String securityCredential){
            if(securityCredential==null||securityCredential.isEmpty())
                throw new IllegalArgumentException("securityCredential must not be null nor empty");
            this.config.setServiceProviderCode(securityCredential);
            return this;
        }

        public Configuration build(){
            if(config.token==null){
                if(apiKey!=null&&publicKeyContent!=null){
                    config.token = makeToken();
                }else throw new IllegalStateException(
                        "You must either set the \"token\" or set both the \"apiKey\" and the \"publicKey\"");
            }
            return config;
        }

        private String makeToken(){
            try {
                KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                Cipher cipher = Cipher.getInstance("RSA");
                byte[] x509PublicKey = Base64.getDecoder().decode(publicKeyContent);
                X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(x509PublicKey);
                PublicKey pk = keyFactory.generatePublic(publicKeySpec);
                cipher.init(Cipher.ENCRYPT_MODE, pk);
                byte[] encryptedApiKey =  Base64.getEncoder().encode(cipher.doFinal(
                        apiKey.getBytes("UTF-8")));
                return new String(encryptedApiKey, "UTF-8");
            } catch (Exception ex) {
                throw new ConfigurationException(ex);
            }
        }
    }

}