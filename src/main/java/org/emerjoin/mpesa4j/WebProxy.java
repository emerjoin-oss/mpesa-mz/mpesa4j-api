package org.emerjoin.mpesa4j;

/**
 * @author Mario Junior.
 */
public class WebProxy {

    private String hostname;
    private int port;

    WebProxy(String hostname, int port){
        this.hostname = hostname;
        this.port = port;
    }

    public String getHostname() {
        return hostname;
    }

    public int getPort() {
        return port;
    }

}
