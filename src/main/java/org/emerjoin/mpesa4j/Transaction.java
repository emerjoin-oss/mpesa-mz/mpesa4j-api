package org.emerjoin.mpesa4j;

/**
 * An {@link MpesaOperation} that causes state changes
 */
public interface Transaction extends MpesaOperation {
    Ref getTransactionRef();
}