package org.emerjoin.mpesa4j.c2b;

/**
 * @author Mario Junior.
 */
public class CustomerWithProblemsException extends IrregularCustomerException {

    public CustomerWithProblemsException() {
        super("Customer's Profile Has Problems","INS-995");
    }
}
