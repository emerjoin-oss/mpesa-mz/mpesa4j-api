package org.emerjoin.mpesa4j.c2b;

import org.emerjoin.mpesa4j.exception.service.response.BusinessException;

public class PaymentApprovalTimeoutException extends BusinessException {

    public PaymentApprovalTimeoutException() {
        super("Customer did not approve the payment on time","INS-6");
    }

}
