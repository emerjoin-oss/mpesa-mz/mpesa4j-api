package org.emerjoin.mpesa4j.c2b;

import org.emerjoin.mpesa4j.exception.service.response.BusinessException;

/**
 * @author Mario Junior.
 */
public class IrregularCustomerException extends BusinessException {

    public IrregularCustomerException(String message, String code) {
        super(message,code);
    }

}
