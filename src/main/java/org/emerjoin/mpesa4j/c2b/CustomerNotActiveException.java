package org.emerjoin.mpesa4j.c2b;

/**
 * @author Mario Junior.
 */
public class CustomerNotActiveException extends IrregularCustomerException {

    public CustomerNotActiveException() {
        super("The customer account is not active","INS-996");
    }
}
