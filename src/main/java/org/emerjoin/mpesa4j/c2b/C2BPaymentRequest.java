package org.emerjoin.mpesa4j.c2b;

import org.emerjoin.mpesa4j.*;

/**
 * Represents the Intention of Collecting a Payment from a customer.
 * @author Mario Junior.
 */
public class C2BPaymentRequest implements Transaction {

    private Ref ref;
    private MobileNumber mobileNumber;
    private Amount amount;

    /**
     * Creates a new {@link C2BPaymentRequest} with an auto-generated transaction-Ref.
     * @param mobileNumber the customer mobile number.
     * @param amount the payment amount.
     */
    public C2BPaymentRequest(MobileNumber mobileNumber, Amount amount){
        this(mobileNumber,amount, Ref.auto());
    }

    /**
     * Creates a new {@link C2BPaymentRequest}.
     * @param mobileNumber the customer mobile number.
     * @param amount the payment amount.
     * @param transactionRef a unique identifier for the transaction.
     */
    public C2BPaymentRequest(MobileNumber mobileNumber, Amount amount, Ref transactionRef){
        CommonValidators.requireNotNull("mobileNumber",mobileNumber);
        CommonValidators.requireNotNull("amount",amount);
        CommonValidators.requireNotNull("transactionRef",transactionRef);
        this.ref = transactionRef;
        this.mobileNumber = mobileNumber;
        this.amount = amount;
    }

    public Ref getTransactionRef() {
        return ref;
    }

    public MobileNumber getMobileNumber() {
        return mobileNumber;
    }

    public Amount getAmount() {
        return amount;
    }

    public static Builder of(Amount amount){
        CommonValidators.requireNotNull("amount",amount);
        return new Builder(amount);
    }

    public static final class Builder{

        private Amount amount;
        private MobileNumber mobileNumber;
        private Ref transactionRef;

        private Builder(Amount amount){
            this.amount = amount;
        }

        public Builder from(MobileNumber mobileNumber){
            CommonValidators.requireNotNull("mobileNumber",mobileNumber);
            this.mobileNumber = mobileNumber;
            return this;
        }

        public Builder withTransactionRef(Ref transactionRef){
            CommonValidators.requireNotNull("transactionRef",transactionRef);
            this.transactionRef = transactionRef;
            return this;
        }

        public C2BPaymentRequest build(){
            if(mobileNumber==null)
                throw new IllegalStateException("mobileNumber not set");
            if(transactionRef==null)
                transactionRef = Ref.auto();
            return new C2BPaymentRequest(mobileNumber,amount,
                    transactionRef);
        }

    }

}
