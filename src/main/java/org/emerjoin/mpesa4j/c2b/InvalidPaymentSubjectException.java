package org.emerjoin.mpesa4j.c2b;

import org.emerjoin.mpesa4j.exception.service.response.business.InvalidTransactionParamException;

public class InvalidPaymentSubjectException extends InvalidTransactionParamException {

    public InvalidPaymentSubjectException(String subject) {
        super("Payment subject is not valid: "+subject,"INS-19");
    }

}
