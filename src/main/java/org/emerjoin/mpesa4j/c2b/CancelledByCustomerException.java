package org.emerjoin.mpesa4j.c2b;

import org.emerjoin.mpesa4j.exception.service.response.BusinessException;

/**
 * @author Mario Junior.
 */
public class CancelledByCustomerException extends BusinessException {

    public CancelledByCustomerException() {
        super("The Customer cancelled the payment request","INS-5");
    }

}
