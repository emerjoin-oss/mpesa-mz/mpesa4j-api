package org.emerjoin.mpesa4j.c2b;

import org.emerjoin.mpesa4j.exception.service.response.BusinessException;

/**
 * @author Mario Junior.
 */
public class InsufficientBalanceException extends BusinessException {

    public InsufficientBalanceException() {
        super("Customer does not have enough balance","INS-2006");
    }

}
