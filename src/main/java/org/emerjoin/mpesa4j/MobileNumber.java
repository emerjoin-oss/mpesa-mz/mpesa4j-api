package org.emerjoin.mpesa4j;

public class MobileNumber {

    private String value;

    public MobileNumber(String value){
        CommonValidators.validateMsisdn(value);
        this.value = value;
    }

    public String toString(){
        return value;
    }

    public static MobileNumber value(String number){
        return new MobileNumber(
                number);
    }


}
