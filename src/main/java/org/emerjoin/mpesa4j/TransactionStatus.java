package org.emerjoin.mpesa4j;

/**
 * @author Mario Junior.
 */
public enum TransactionStatus {
    COMPLETED, CANCELLED, EXPIRED
}
