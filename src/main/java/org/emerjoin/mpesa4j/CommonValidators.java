package org.emerjoin.mpesa4j;

import java.util.regex.Pattern;

public final class CommonValidators {

    private static final Pattern MSISDN_PATTERN = Pattern.compile("^(8[45])[0-9]{7}$");
    private static final Pattern REF_PATTERN = Pattern.compile("^[a-zA-Z0-9]{1,20}$");
    private static final Pattern SUBJECT_PATTERN = Pattern.compile("^[a-zA-Z0-9]{1,30}$");

    public static void requireNotNull(String name, Object value){
        if(value==null)
            throw new IllegalArgumentException(String.format("%s must not be null",
                    name));
    }

    public static void validateMsisdn(String msisdn){
        if(msisdn==null||msisdn.isEmpty()||!MSISDN_PATTERN.matcher(msisdn).matches())
            throw new IllegalArgumentException("msisdn must not be null nor empty and must have 9 digits, starting with either 84 or 85");
    }

    public static void validateRef(String ref, String designation){
        if(ref==null||ref.isEmpty()||!REF_PATTERN.matcher(ref).matches())
            throw new IllegalArgumentException(designation+" must not be null nor empty and must have no spaces nor special characters and must have a maximum of 20 characters");
    }

    public static void validateSubject(String subject){
        if(subject==null||subject.isEmpty())
            throw new IllegalArgumentException("payment subject must not be null nor empty");
        if(!SUBJECT_PATTERN.matcher(subject).matches())
            throw new IllegalArgumentException("Payment subject must not contain any spaces or special characters and must have a maximum of 30 characters");
    }

}
