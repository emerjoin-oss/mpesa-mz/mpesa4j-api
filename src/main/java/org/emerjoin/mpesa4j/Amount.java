package org.emerjoin.mpesa4j;

public class Amount {

    private double value;

    public Amount(double amount){
        if(amount<0)
            throw new IllegalArgumentException("amount must be higher than zero");
        this.value = amount;
    }

    public static Amount of(double value){
        return new Amount(value);
    }

    public double value(){
        return value;
    }

}
