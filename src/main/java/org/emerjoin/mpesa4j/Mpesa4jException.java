package org.emerjoin.mpesa4j;

/**
 * @author Mario Junior.
 */
public class Mpesa4jException extends RuntimeException {

    public Mpesa4jException(String message, Throwable cause){
        super(message,cause);
    }

    public Mpesa4jException(String message){
        super(message);
    }

}
